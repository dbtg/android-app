package pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.activities;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.activities.shift.MultiSelectionSpinner;
import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.activities.main.Plan;
import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.activities.main.adapters.PlanRecyclerViewAdapter;
import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.R;
import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.models.api.Block;
import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.helpers.DateHelper;

import static java.sql.Types.NULL;

/**
 * This activity is unused
 */
public class ShiftActivity extends AppCompatActivity {


private String selected_date;
private boolean auto_selected_once=false;
private boolean manual_selected_once=false;
private ArrayList<Plan> manualPlan = new ArrayList<>();
private RecyclerView.Adapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shift);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        final LinearLayout auto_shift_layout= (LinearLayout) findViewById(R.id.auto_shift_layout);
        final LinearLayout manual_shift_layout= (LinearLayout) findViewById(R.id.manual_shift_layout);
        final TabLayout tabLayout=(TabLayout)findViewById(R.id.switch_shift);
        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        PagerAdapter adapter = new PagerAdapter() {
            @Override
            public int getCount() { return tabLayout.getTabCount(); }

            @Override
            public boolean isViewFromObject(View view, Object object) {
                getSupportFragmentManager();
                return false;
            }
        };
        tabLayout.setupWithViewPager(viewPager);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if(tab.getPosition()==0){
                    manual_shift_layout.setVisibility(View.GONE);
                    auto_shift_layout.setVisibility(View.VISIBLE);
                    if(auto_selected_once==false)
                    {
                        autoSelected();
                        auto_selected_once=true;
                    }
                }
                else{
                    auto_shift_layout.setVisibility(View.GONE);
                    manual_shift_layout.setVisibility(View.VISIBLE);
                    if(manual_selected_once==false) {
                        manualSelected();
                        manual_selected_once = true;
                    }
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                //everything is done in TabSelected
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                //do nothing
            }
        });
    }


    public void autoSelected(){
        MultiSelectionSpinner spinner=(MultiSelectionSpinner)findViewById(R.id.chooseRoom);
        List<String> roomList = new ArrayList<String>();
        roomList.add("List1");
        roomList.add("List2");
        spinner.setItems(roomList);

        spinner=(MultiSelectionSpinner)findViewById(R.id.chooseLecturer);
        List<String> lecturerList = new ArrayList<String>();
        lecturerList.add("List3");
        lecturerList.add("List4");
        spinner.setItems(lecturerList);

        spinner=(MultiSelectionSpinner)findViewById(R.id.chooseGroups);
        List<String> groupList = new ArrayList<String>();
        groupList.add("List5");
        groupList.add("List6");
        spinner.setItems(groupList);

        Button findShift=(Button)findViewById(R.id.findShift);
        findShift.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //find avaible blocks
            }
        });
    }

    public void manualSelected(){
        final CalendarView calendar= (CalendarView)findViewById(R.id.chooseDay);
        final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.manual_daily_list);
        calendar.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                manualPlan.clear();
                String StrMonth;
                if (month<11){
                    StrMonth = "0" + Integer.toString(month+1);
                }
                else
                    StrMonth=Integer.toString(month+1);
                String day;
                if (dayOfMonth<10){
                    day = "0" + dayOfMonth;
                }
                else
                    day=Integer.toString(dayOfMonth);

                String fragmentDate = Integer.toString(year) + '-' + StrMonth + '-' + day;
                int nextBlock = 0;

                for (Block block : MainActivity.blocks) {
                    String blockDate = block.getDate();
                    if (blockDate.equals(fragmentDate)) {
                        if(block.getBlock() > nextBlock){
                            for (int i = nextBlock; i < block.getBlock(); i++){
                                Plan test= new Plan("","","", DateHelper.getBlockStart(i), DateHelper.getBlockEnd(i),NULL);
                                manualPlan.add(test);
                            }
                        }
                        nextBlock = block.getBlock() + 1;
                        Plan test = new Plan(block.getSubject(), block.getType(), block.getRoom(), block.getBlockStart(), block.getBlockEnd(), Color.rgb(0,150,10));
                        manualPlan.add(test);
                    }
                }

                mAdapter = new PlanRecyclerViewAdapter(manualPlan);
                recyclerView.setAdapter(mAdapter);
                recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
            }
        });
    }
}
