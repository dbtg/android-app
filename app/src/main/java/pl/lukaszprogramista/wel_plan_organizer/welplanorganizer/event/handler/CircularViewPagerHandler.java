package pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.event.handler;

import android.support.v4.view.ViewPager;

import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.activities.main.adapters.PlanPagerAdapter;

/**
 * OnPageChangeListener that allows ViewPager infinite scrolling
 */
public class CircularViewPagerHandler implements ViewPager.OnPageChangeListener {
    public static final int                 SET_ITEM_DELAY = 200;

    private ViewPager                       mViewPager;
    private ViewPager.OnPageChangeListener  mListener;

    public CircularViewPagerHandler(final ViewPager viewPager) {
        mViewPager = viewPager;
    }

    public void setOnPageChangeListener(final ViewPager.OnPageChangeListener listener) {
        mListener = listener;
    }

    @Override
    public void onPageSelected(final int position) {
        handleSetCurrentItemWithDelay(position);
    }

    private void handleSetCurrentItemWithDelay(final int position) {
        mViewPager.postDelayed(new Runnable() {
            @Override
            public void run() {
                handleSetCurrentItem(position);
            }
        }, SET_ITEM_DELAY);
    }

    private void handleSetCurrentItem(final int position) {
        ((PlanPagerAdapter)mViewPager.getAdapter()).updateDayPlans(position);
    }

    @Override
    public void onPageScrollStateChanged(final int state) {
        invokeOnPageScrollStateChanged(state);
    }

    private void invokeOnPageScrollStateChanged(final int state) {
        if(mListener != null) {
            mListener.onPageScrollStateChanged(state);
        }
    }

    @Override
    public void onPageScrolled(final int position, final float positionOffset, final int positionOffsetPixels) {
        invokeOnPageScrolled(position, positionOffset, positionOffsetPixels);
    }

    private void invokeOnPageScrolled(final int position, final float positionOffset, final int positionOffsetPixels) {
        if(mListener != null) {
            mListener.onPageScrolled(position - 1, positionOffset, positionOffsetPixels);
        }
    }
}