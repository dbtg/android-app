package pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.helpers;

import android.graphics.Color;
import android.util.Log;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.OrganizerApplication;
import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.R;
import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.activities.MainActivity;
import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.models.api.RestListResponse;
import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.models.api.RestPlanResponse;
import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.models.database.Block;
import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.models.database.BlockDao;
import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.models.database.Group;
import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.models.database.GroupDao;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Helper for Api tasks like updates enquing etc
 */
public class ApiHelper {

    /**
     * Load certain group data from API to local database
     * @param groupName
     */
    public static void updateGroup(final String groupName){
        OrganizerApplication.getApiService().getPlan(groupName).enqueue(new Callback<RestPlanResponse>() {
            @Override
            public void onResponse(Call<RestPlanResponse> call, Response<RestPlanResponse> response) {

                try {
                    if(response.isSuccessful()) {
                        //get group id
                        Group group = Group.getGroupByName(groupName);

                        //blocks
                        List<pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.models.api.Block> apiBlocks = response.body().getBlocks();
                        List<Block> dbBlocks = new ArrayList<Block>();
                        ColorHelper colorHelper = new ColorHelper();
                        for (pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.models.api.Block apiBlock : apiBlocks){
                            Block dbBlock = new Block(null, group.getId(),apiBlock.getBlock(), apiBlock.getDate(),
                                    apiBlock.getType(),apiBlock.getExtra(),apiBlock.getRoom(), apiBlock.getLecturers(),
                                    colorHelper.getColor(apiBlock.getSubject()), apiBlock.getShort(), apiBlock.getSubject());
                            dbBlocks.add(dbBlock);
                        }
                        OrganizerApplication.getDaoSession().getBlockDao().queryBuilder().where(BlockDao.Properties.GroupId.eq(group.getId())).buildDelete().executeDeleteWithoutDetachingEntities();
                        OrganizerApplication.getDaoSession().clear();
                        OrganizerApplication.getDaoSession().getBlockDao().insertInTx(dbBlocks);

                        //mPlanPagerAdapter.notifyPlanLoaded();
                        Toast.makeText(OrganizerApplication.getContext(), OrganizerApplication.getContext().getString(R.string.plan_for_group_was_loaded) + groupName, Toast.LENGTH_SHORT).show();
                    } else {
                        int statusCode  = response.code();
                        Toast.makeText(OrganizerApplication.getContext(), OrganizerApplication.getContext().getString(R.string.no_server_connection), Toast.LENGTH_LONG).show();
                    }
                }
                catch (Exception e){
                    Toast.makeText(OrganizerApplication.getContext(), OrganizerApplication.getContext().getString(R.string.error_while_updating_group) + groupName, Toast.LENGTH_SHORT).show();
                    return;
                }

            }

            @Override
            public void onFailure(Call<RestPlanResponse> call, Throwable t) {
                Log.d("PLAN API CALL", "error loading from API");
                Toast.makeText(OrganizerApplication.getContext(), OrganizerApplication.getContext().getString(R.string.no_server_connection), Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * Update all groups existing in the database
     */
    public static void updateGroups(){
        final List<Group>groupList = OrganizerApplication.getDaoSession().getGroupDao().loadAll();
        if (groupList.size() > 0){
            //check server connection
            OrganizerApplication.getApiService().getGroups().enqueue(new Callback<RestListResponse>() {
                @Override
                public void onResponse(Call<RestListResponse> call, Response<RestListResponse> response) {
                    for (Group group : groupList){
                        updateGroup(group.getName());
                    }
                }

                @Override
                public void onFailure(Call<RestListResponse> call, Throwable t) {
                    Toast.makeText(OrganizerApplication.getContext(), R.string.update_error_no_internet, Toast.LENGTH_LONG).show();
                }
            });
        }
    }
}
