package pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.models.viewpager;

import org.greenrobot.greendao.query.Join;
import org.greenrobot.greendao.query.QueryBuilder;
import org.greenrobot.greendao.query.WhereCondition;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.OrganizerApplication;
import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.models.database.Block;
import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.models.database.BlockDao;
import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.models.database.Group;
import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.models.database.GroupDao;

public class DayPlan {
    public Calendar getCalendar() {
        return calendar;
    }

    private Calendar calendar;

    private String groupName;

    private List<Block> blocks;

    public DayPlan(Calendar c, String gn){
        groupName = gn;
        calendar = c;
    }

    public static DayPlan generateNextDay(DayPlan d){
        Calendar c = (Calendar) d.getCalendar().clone();
        c.add(Calendar.DATE,1);
        return new DayPlan(c,d.getGroupName());
    }

    public static DayPlan generatePreviousDay(DayPlan d){
        Calendar c = (Calendar) d.getCalendar().clone();
        c.add(Calendar.DATE,-1);
        return new DayPlan(c,d.getGroupName());
    }

    public static List<DayPlan> generatePlanList(Calendar c, String gn){
        List<DayPlan> list = new ArrayList<DayPlan>();
        c = (Calendar) c.clone();
        c.add(Calendar.DATE, -1);
        list.add(new DayPlan(c, gn));
        c = (Calendar) c.clone();
        c.add(Calendar.DATE,1);
        list.add(new DayPlan(c, gn));
        c = (Calendar) c.clone();
        c.add(Calendar.DATE,1);
        list.add(new DayPlan(c, gn));
        return list;
    }

    public static List<DayPlan> generateTodayPlanList(String gn) {
        Calendar c = Calendar.getInstance();
        return generatePlanList(c, gn);
    }

    public String getGroupName() {
        return groupName;
    }

    public List<Block> getBlocks() {
        if (blocks == null){
            calendar.clear(Calendar.ZONE_OFFSET);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);
            Calendar tmpCalendar = (Calendar) calendar.clone();
            tmpCalendar.add(Calendar.DATE, -4);
            Group g = Group.getGroupByName(getGroupName());
            if (g == null){
                return null;
            }

            String date = calendar.get(Calendar.YEAR) + "-" + ((calendar.get(Calendar.MONTH) + 1) < 10 ? "0" : "") + (calendar.get(Calendar.MONTH) + 1) + "-" + calendar.get(Calendar.DATE);

            QueryBuilder<Block> blockQueryBuilder = OrganizerApplication.getDaoSession().getBlockDao().queryBuilder()
                    .where(
                            BlockDao.Properties.GroupId.eq(g.getId()),
                            BlockDao.Properties.Date.eq(date)
                    );
            //blockQueryBuilder.where(BlockDao.Properties.Date.eq(calendar.getTimeInMillis()));
            blocks = blockQueryBuilder.list();
        }

        Date test = calendar.getTime();

        return blocks;
    }
}
