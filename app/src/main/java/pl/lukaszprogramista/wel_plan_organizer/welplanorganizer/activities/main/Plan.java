package pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.activities.main;

public class Plan {
    private String name;
    private String type;
    private String room;
    private String startHour;
    private String endHour;
    private int background;

    /**
     * Constructor of data container for plan's recycler view
     * @param name
     * @param type
     * @param room
     * @param startHour
     * @param endHour
     * @param background
     */
    public Plan(String name, String type,String room,String startHour,String endHour, int background){
        this.name=name;
        this.type=type;
        this.room=room;
        this.startHour=startHour;
        this.endHour=endHour;
        this.background=background;
    }

    public String getName() {return name;}
    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }

    public String getRoom() {
        return room;
    }
    public void setRoom(String room) {
        this.room = room;
    }

    public String getStartHour() {
        return startHour;
    }
    public void setStartHour(String startHour) {
        this.startHour = startHour;
    }

    public String getEndHour() {
        return endHour;
    }
    public void setEndHour(String endHour) {
        this.endHour = endHour;
    }

    public int getBackground() {
        return background;
    }
    public void setBackground(int background) {
        this.background = background;
    }
}
