package pl.lukaszprogramista.wel_plan_organizer.welplanorganizer;

import android.app.Application;
import android.content.Context;

import org.greenrobot.greendao.database.Database;

import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.helpers.ApiHelper;
import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.models.database.DaoMaster;
import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.models.database.DaoSession;
import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.services.ApiService;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Organizer application
 */
public class OrganizerApplication extends Application {

    /**
     * API base url for retrofit
     */
    public static final String API_BASE_URL = "http://vps214967.ovh.net:31337";

    /**
     * Get Retrofit ApiService instance
     *
     * @return ApiService Retrofit Client
     */
    public static ApiService getApiService() {
        return apiService;
    }

    private static ApiService apiService;

    /**
     * get GreenDao ORM Session
     * @return DaoSession reference
     */
    public static DaoSession getDaoSession() {
        return daoSession;
    }

    private static DaoSession daoSession;

    /**
     * get application instance
     * @return application instance reference
     */
    public static Application getInstance() {
        return instance;
    }

    private static Application instance;

    /**
     * Initialize extra services on app startup
     */
    @Override
    public void onCreate() {
        super.onCreate();

        //singleton
        instance = this;

        //initialize retrofit API service
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.plan_api_url))
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        apiService = retrofit.create(ApiService.class);

        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "plan-db");
        Database db = helper.getWritableDb();
        daoSession = new DaoMaster(db).newSession();

        //update local database
        ApiHelper.updateGroups();
    }

    public static Context getContext() {
        return instance.getApplicationContext();
    }
}
