package pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.activities.main.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.activities.main.fragments.DayPlanFragment;
import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.models.viewpager.DayPlan;

import static pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.helpers.DateHelper.compareCalendars;

/**
 * Adapter for ViewPager with 'infinite' scroll hack
 * Adapter is holding only 3 fragments and juggling them while scrolling
 */
public class PlanPagerAdapter extends FragmentStatePagerAdapter
{
    public static int LOOPS_COUNT = 1000; // for infinite scrolling
    private List<DayPlan> dayPlans;
    private List<Fragment> fragments; //references to existing
    private FragmentManager fm;

    /**
     * Plan Pager adapter constructor
     * @param manager
     * @param dp list of first three plans to load
     */
    public PlanPagerAdapter(FragmentManager manager, List<DayPlan> dp)
    {
        super(manager);
        dayPlans = dp;
        fm = manager;
        fragments = new ArrayList<Fragment>();

        //initialize 3 fragments to load data to them later
        fragments.add(new Fragment());
        fragments.add(new Fragment());
        fragments.add(new Fragment());
    }

    /**
     * Change plan on existing fragments
     * @param dp list of plans to apply
     */
    public void setPlanAndUpdate(List<DayPlan> dp){
        dayPlans = dp;
        notifyDataSetChanged();
        //update recyclers
        for (Fragment fragment : fm.getFragments()){
            if (fragment instanceof  DayPlanFragment) {
                for (DayPlan dayPlan : dayPlans) {
                    if (compareCalendars(((DayPlanFragment) fragment).getDayPlan().getCalendar(),dayPlan.getCalendar())) {
                        ((DayPlanFragment) fragment).loadNewPlan(dayPlan);
                    }
                }
            }
        }
    }

    /**
     * Get Fragment with 'infinite scrolling' hack applied
     * @param position index
     * @return
     */
    @Override
    public Fragment getItem(int position)
    {
        if (dayPlans != null && dayPlans.size() > 0)
        {
            position = position % dayPlans.size(); // use modulo for infinite cycling
            Fragment fragment = DayPlanFragment.instantiateWithArgs(dayPlans.get(position));
            fragments.set(position,fragment);
            return DayPlanFragment.instantiateWithArgs(dayPlans.get(position));
        }
        else
        {
            Fragment fragment = DayPlanFragment.instantiateWithArgs(null);
            fragments.set(position,fragment);
            return fragment;
        }
    }


    /**
     * Get not really real count of fragments - infinite scrolling hack
     * @return
     */
    @Override
    public int getCount()
    {
        if (dayPlans != null && dayPlans.size() > 0)
        {
            return dayPlans.size()*LOOPS_COUNT; // simulate infinite by big number of products
        }
        else
        {
            return 1;
        }
    }

    /**
     * Get day plan by Fake position (infinite scroll hack)
     * @param fakePosition position that ViePager thinks it is
     */
    public DayPlan getDayPlanByFakePosition(int fakePosition){
        int realPosition = fakePosition % dayPlans.size();
        return dayPlans.get(realPosition);
    }

    /**
     * Update day plan by real position
     * @param position
     */
    public void updateDayPlans(int position) {
        position = position % dayPlans.size();
        DayPlan currentPlan = dayPlans.get(position);
        if (position == 2) {
            dayPlans.set(0,DayPlan.generateNextDay(currentPlan));
            dayPlans.set(1,DayPlan.generatePreviousDay(currentPlan));
        } else if (position == 0) {
            dayPlans.set(1,DayPlan.generateNextDay(currentPlan));
            dayPlans.set(2,DayPlan.generatePreviousDay(currentPlan));
        }else if (position == 1) {
            dayPlans.set(2,DayPlan.generateNextDay(currentPlan));
            dayPlans.set(0,DayPlan.generatePreviousDay(currentPlan));
        }
    }
}
