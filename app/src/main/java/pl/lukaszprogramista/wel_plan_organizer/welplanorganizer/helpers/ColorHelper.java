package pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.helpers;

import android.graphics.Color;
import android.util.Log;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class ColorHelper {

    /**
     * Preset array of colors to be used in plan fragments
     */
    private static final Integer ColorList[] = {
            Color.argb(220,72,186,0),
            Color.argb(220,200,255,230),
            Color.argb(220,250,0,0),
            Color.argb(220,0,250,0),
            Color.argb(220,0,250,250),
            Color.argb(220,128,128,128),
            Color.argb(220,128,0,128),
            Color.argb(220,0,128,128),
            Color.argb(220,220,25,60),
            Color.argb(220,255,70,0),
            Color.argb(220,255,140,0),
            Color.argb(220,0,128,0),
            Color.argb(220,50,128,50),
            Color.argb(220,0,250,158),
            Color.argb(220,0,250,170),
            Color.argb(220,200,255,230),
            Color.argb(220,127,255,212),
            Color.argb(220,0,250,0),
            Color.argb(220,0,190,250),
            Color.argb(220,140,40,226),
            Color.argb(220,128,0,128),
            Color.argb(220,210,105,30),
            Color.argb(220,170,145,140),
            Color.argb(220,170,200,222),
            Color.argb(220,150,250,250),
            Color.argb(220,169,169,169),

            Color.argb(220,72,186,0),
            Color.argb(220,200,255,230),
            Color.argb(220,250,0,0),
            Color.argb(220,0,250,0),
            Color.argb(220,0,250,250),
            Color.argb(220,128,128,128),
            Color.argb(220,128,0,128),
            Color.argb(220,0,128,128),
            Color.argb(220,220,25,60),
            Color.argb(220,255,70,0),
            Color.argb(220,255,140,0),
            Color.argb(220,0,128,0),
            Color.argb(220,50,128,50),
            Color.argb(220,0,250,158),
            Color.argb(220,0,250,170),
            Color.argb(220,200,255,230),
            Color.argb(220,127,255,212),
            Color.argb(220,0,250,0),
            Color.argb(220,0,190,250),
            Color.argb(220,140,40,226),
            Color.argb(220,128,0,128),
            Color.argb(220,210,105,30),
            Color.argb(220,170,145,140),
            Color.argb(220,170,200,222),
            Color.argb(220,150,250,250),
            Color.argb(220,169,169,169),
    };

    private LinkedHashMap<String, Integer> colorMap=new LinkedHashMap<String, Integer>();
    private int colorIterator=0;

    /**
     * Get color that is unique for a subject
     * @param subject
     * @return
     */
    public int getColor(String subject){
        if(colorMap.containsKey(subject)){
            return colorMap.get(subject);
        }
        else{
            try {
                colorMap.put(subject, ColorList[colorIterator]);
            } catch (Exception e){
                Log.d("color selector error","asd");
            }
            colorIterator++;
            return colorMap.get(subject);
        }
    }

    /**
     * Should be used before parasing every group, if all groups are using
     * same instance of color helper
     */
    public void clearColors(){
        colorMap.clear();
    }
}
