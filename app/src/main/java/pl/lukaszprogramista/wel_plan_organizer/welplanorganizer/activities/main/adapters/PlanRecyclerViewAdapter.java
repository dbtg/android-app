package pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.activities.main.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.R;
import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.activities.main.Plan;

/**
 * Adapter is having a dirty hack for long press click.
 * Long press is being handled by MainActivity onContext... methods
 */
public class PlanRecyclerViewAdapter extends RecyclerView.Adapter<PlanRecyclerViewAdapter.ViewHolder> {

    List<Plan> blocks;
    public PlanRecyclerViewAdapter(List<Plan> blocks) {
        this.blocks=blocks;
    }

    private static int position = -1; // for keeping track of which block on recycler view was clicked
    public static int getPosition() {
        return position;
    }
    public static void setPosition(int _position) {
        position = _position;
    }

    /**
     * On Create Event handler - initializing some variables
     * @param parent
     * @param viewType
     * @return
     */
    @Override
    public PlanRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.subject_list,parent,false);
        ViewHolder viewHolder=new ViewHolder(v);
        position = -1; // by default none of blocks on recycler view was clicked
        return viewHolder;
    }

    /**
     * Set Recycler View data
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(final PlanRecyclerViewAdapter.ViewHolder holder, int position) {
        holder.name.setText(blocks.get(position).getName());
        holder.type.setText(blocks.get(position).getType());
        holder.room.setText(blocks.get(position).getRoom());
        holder.startHour.setText(blocks.get(position).getStartHour());
        holder.endHour.setText(blocks.get(position).getEndHour());
        holder.linear.setBackgroundColor(blocks.get(position).getBackground());
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                setPosition(holder.getAdapterPosition());
                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return blocks.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener{
        private LinearLayout linear;
        private TextView name;
        private TextView type;
        private TextView room;
        private TextView startHour;
        private TextView endHour;

        private ViewHolder(final View itemView) {
            super(itemView);
            itemView.setOnCreateContextMenuListener(this);
            linear=(LinearLayout)itemView.findViewById(R.id.subject_list_View);
            name= (TextView) itemView.findViewById(R.id.subjectListName);
            type= (TextView) itemView.findViewById(R.id.subjectListType);
            room= (TextView) itemView.findViewById(R.id.subjectListRoom);
            startHour= (TextView) itemView.findViewById(R.id.subjectListStartHour);
            endHour= (TextView) itemView.findViewById(R.id.subjectListEndingHour);
        }
        @Override
        public void onCreateContextMenu(ContextMenu menu, View v,
                                        ContextMenu.ContextMenuInfo menuInfo) {
            //menuInfo todo: subject details
            menu.add(Menu.NONE, 1, Menu.NONE, "Close");
        }
    }
}
