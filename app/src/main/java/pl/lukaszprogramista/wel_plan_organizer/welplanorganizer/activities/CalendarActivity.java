package pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.CalendarView;

import java.util.Calendar;

import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.R;

public class CalendarActivity extends AppCompatActivity {

    /**
     * Basically initializing the activity and setting date
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        CalendarView calendar= (CalendarView)findViewById(R.id.calendar);
        calendar.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                passDatetoMain(dayOfMonth,month,year);
                finishAffinity();
            }
        });
    }

    /**
     * Go back to main activity and load certain date
     * @param day
     * @param month 0-indexed
     * @param year
     */
    protected void passDatetoMain(int day, int month, int year){
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("day", day);
        intent.putExtra("month", month);
        intent.putExtra("year", year);
        startActivity(intent);
    }
}
