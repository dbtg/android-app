package pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.activities;

import android.support.v4.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.SubMenu;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.OrganizerApplication;
import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.activities.main.adapters.PlanPagerAdapter;
import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.R;
import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.activities.main.adapters.PlanRecyclerViewAdapter;
import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.activities.main.fragments.GroupDialogFragment;
import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.event.handler.CircularViewPagerHandler;
import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.helpers.ApiHelper;
import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.models.api.Block;
import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.models.api.Group;
import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.models.api.RestListResponse;
import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.models.viewpager.DayPlan;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.helpers.PreferencesHelper;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    /**
     * Load certain day on the plan
     * @param c
     */
    public void loadDay(Calendar c) {
        String defaultGroup = PreferencesHelper.getDefaultGroup();
        if (mPlanPagerAdapter == null && defaultGroup == null ){
            Toast.makeText(instance, R.string.need_to_choose_plan_first, Toast.LENGTH_SHORT).show();
            return;
        }
        if (mPlanPagerAdapter == null) {
            //initialize pager
            mPlanPagerAdapter = new PlanPagerAdapter(getSupportFragmentManager(), DayPlan.generatePlanList(c, defaultGroup));
            mViewPager.setAdapter(mPlanPagerAdapter);
            mViewPager.setCurrentItem(3 * mPlanPagerAdapter.LOOPS_COUNT / 2 + 1, false);
            mViewPager.addOnPageChangeListener(new CircularViewPagerHandler(mViewPager));
        } else {
            //update pager
            mPlanPagerAdapter.setPlanAndUpdate(DayPlan.generatePlanList(c, defaultGroup));
            mViewPager.setCurrentItem(3 * mPlanPagerAdapter.LOOPS_COUNT / 2 + 1, false);
        }
    }

    private enum ContextMenuType {
        GROUP,LECTURER,ROOM
    }

    //singleton
    private static MainActivity instance;

    /**
     * Get instance of main activity (singleton)
     * @return
     */
    public static MainActivity getInstance() {
        return instance;
    }

    //group id for dynamically generated menu items in navbar menus
    private static final int GROUP_GROUP_ID = 131;
    //private static final int LECTURERS_GROUP_ID = 132;
    //static final int ROOMS_GROUP_ID = 133;

    public ContextMenuType currentContextMenu;
    public static List<Group> groups = new ArrayList<Group>();
    public static List<Block> blocks = new ArrayList<Block>();

    private PlanPagerAdapter mPlanPagerAdapter;
    private ViewPager mViewPager;

    /**
     * Context menu, navigation and singleton initialization
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        registerForContextMenu(navigationView);

        instance = this;

    }

    /**
     * On Activity Start update Plan and load currently/last used plan (if one is set)
     */
    @Override
    protected void onStart() {
        super.onStart();
        loadGroups();
        ApiHelper.updateGroups();
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setOffscreenPageLimit(0);

        String defaultGroup = PreferencesHelper.getDefaultGroup();

        if (defaultGroup != null && pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.models.database.Group.getGroupByName(defaultGroup) != null) {
            Bundle extras = getIntent().getExtras();
            if(extras!=null && extras.getInt("year") != 0){
                Calendar c = (Calendar) Calendar.getInstance().clone();
                c.set(Calendar.YEAR, extras.getInt("year"));
                c.set(Calendar.MONTH, extras.getInt("month"));
                c.set(Calendar.DATE, extras.getInt("day"));
                loadDay(c);
            } else {
                loadCurrentGroupPlan();
            }
        }
        reloadSidebarSubMenus();
    }

    /**
     * Reload all the dynamically loaded items in menu - to be used after deleting
     * or adding something (for instance a group)
     */
    public void reloadSidebarSubMenus(){
        NavigationView navView = (NavigationView) findViewById(R.id.nav_view);
        Menu m = navView.getMenu();
        //get first submenu which are: groups
        SubMenu sm = m.getItem(0).getSubMenu();

        //clear all current items:
        while (sm.size() > 1){
            MenuItem mi = sm.getItem(0);
            if (mi.getItemId() != R.id.addGroup) {
                sm.removeItem(mi.getItemId());
            }
        }
        int i = 0;
        //register an item remove dialog onLongPress
        for (final pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.models.database.Group group : OrganizerApplication.getDaoSession().getGroupDao().loadAll()){
            View v = new View(this);
            v.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    DialogFragment dialogFragment = new GroupDialogFragment();

                    Bundle args = new Bundle();
                    args.putString(getString(R.string.settings_current_group),group.getName());
                    dialogFragment.setArguments(args);

                    dialogFragment.show(getSupportFragmentManager(),"removeGroup");
                    return true;
                }

            });
            MenuItem groupItem = sm.add(GROUP_GROUP_ID, i++, 0, group.getName());
            groupItem.setActionView(v);
        }

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    /**
     * Menu inflating
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    /**
     * Handling all the context menus
     * @param menu
     * @param v
     * @param menuInfo
     */
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        if(v.getId()==R.id.nav_view){
            if(currentContextMenu == ContextMenuType.GROUP){
                if (groups.isEmpty()) {
                    Toast.makeText(this, getString(R.string.no_server_connection), Toast.LENGTH_LONG).show();
                    loadGroups();
                    return;
                }
                MenuInflater inflater = getMenuInflater();
                inflater.inflate(R.menu.add_favourite_context_menu, menu);
                for ( Group group : groups) {
                    menu.add(group.getName());
                }
            }
            /*if(currentContextMenu == ContextMenuType.LECTURER){
                MenuInflater inflater = getMenuInflater();
                inflater.inflate(R.menu.add_favourite_context_menu, menu);
                menu.add("dr inż. Mariusz Wierzbowski");
                menu.add("dr hab. inż. Dariusz Laskowski");
            }
            if(currentContextMenu == ContextMenuType.ROOM){
                MenuInflater inflater = getMenuInflater();
                inflater.inflate(R.menu.add_favourite_context_menu, menu);
                menu.add("117/45");
                menu.add("118/45");
            }*/
        }

    }

    /**
     * Loading available groups for future adding to menu
     */
    public void loadGroups(){
        OrganizerApplication.getApiService().getGroups().enqueue(new Callback<RestListResponse>() {
            @Override
            public void onResponse(Call<RestListResponse> call, Response<RestListResponse> response) {

                if(response.isSuccessful()) {
                    groups = response.body().getGroups();
                }else {
                    int statusCode  = response.code();
                    // handle request errors depending on status code
                }
            }

            @Override
            public void onFailure(Call<RestListResponse> call, Throwable t) {
                Log.d("MainActivity", "error loading from API");
            }
        });
    }

    /**
     * Java sucks, default values in arguments should be allowed
     */
    public void loadCurrentGroupPlan(){
        loadGroupPlan(null);
    }

    /**
     * Load plan by group name
     * @param groupName name of the group
     */
    public void loadGroupPlan(String groupName){
        if (groupName == null) {
            groupName = PreferencesHelper.getDefaultGroup();
            if (groupName == null && mPlanPagerAdapter != null) {
                return;
            }
        }
        if (mPlanPagerAdapter == null) {
            mPlanPagerAdapter = new PlanPagerAdapter(getSupportFragmentManager(), DayPlan.generateTodayPlanList(groupName));
            mViewPager.setAdapter(mPlanPagerAdapter);
            mViewPager.setCurrentItem(3 * mPlanPagerAdapter.LOOPS_COUNT / 2 + 1, false); // infinite scrolling hack
            mViewPager.addOnPageChangeListener(new CircularViewPagerHandler(mViewPager));
        } else {
            mPlanPagerAdapter.setPlanAndUpdate(DayPlan.generatePlanList(mPlanPagerAdapter.getDayPlanByFakePosition(mViewPager.getCurrentItem()).getCalendar(), groupName)); // infinite scrolling hack
            mViewPager.setCurrentItem(3 * mPlanPagerAdapter.LOOPS_COUNT / 2 + 1, false);
        }

    }

    /**
     * Handling recycler view item long press implementation
     * @param menu
     */
    @Override
    public void onContextMenuClosed(Menu menu){
        try {
            if (PlanRecyclerViewAdapter.getPosition() != -1) {
                PlanRecyclerViewAdapter.setPosition(-1);
            }
        } catch (Exception e) {
        }
    }

    /**
     * Context menu handler for adding new groups and recycler view long press menu
     * @param item
     * @return
     */
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        //recycler onLongPress context menu
        try {
            int position = PlanRecyclerViewAdapter.getPosition();
            if (position != -1){
                switch (item.getItemId()) {
                    case 1:
                        Log.e("PlanTest", "RecyclerClick");
                        break;
                }
                return true;
            }
        } catch (Exception e) {
        }

        //menu for adding stuff
        switch (currentContextMenu) {
            case GROUP:
                pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.models.database.Group group = new pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.models.database.Group(null, item.getTitle().toString());
                try {
                    //this model has un unique group name, duplicating a group will throw an exception
                    OrganizerApplication.getDaoSession().getGroupDao().insert(group);
                } catch (Exception e) {
                }

                //save this group as current (aka default)
                PreferencesHelper.setDefaultGroup(group.getName());
                reloadSidebarSubMenus();
                break;
            default:
        }
        return true;
    }

    /**
     * Options Menu handler
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_calendar) {
            Intent intent = new Intent(this, CalendarActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Left side navigation menu handling
     * @param item
     * @return
     */
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        int groupId = item.getGroupId();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        //if new group was selected

        if (groupId == GROUP_GROUP_ID){
            //save this group as current (aka default)
            PreferencesHelper.setDefaultGroup(item.getTitle().toString());
            ApiHelper.updateGroup(item.getTitle().toString());
            loadGroupPlan(item.getTitle().toString());

            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);
            return true;
        }

        if (id == R.id.addGroup) {
            currentContextMenu = ContextMenuType.GROUP;
            openContextMenu(navigationView);

        } else if (id == R.id.check_updates) {
            ApiHelper.updateGroups();
        } else if (id == R.id.open_calendar) {
            Intent intent = new Intent(this, CalendarActivity.class);
            startActivity(intent);
        } else if (id == R.id.exit) {
            this.finishAffinity();
            System.exit(0);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
