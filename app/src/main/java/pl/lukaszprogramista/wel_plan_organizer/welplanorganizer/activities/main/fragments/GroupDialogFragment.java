package pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.activities.main.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.Toast;

import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.OrganizerApplication;
import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.R;
import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.activities.MainActivity;
import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.models.database.Group;
import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.models.database.GroupDao;

public class GroupDialogFragment extends DialogFragment {

    /**
     * Dialog event handling for removing groups
     * @param savedInstanceState
     * @return
     */
    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.remove_group_question)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        String groupName = getArguments().getString(getString(R.string.settings_current_group));

                        //remove from db
                        OrganizerApplication.getDaoSession().queryBuilder(Group.class)
                                .where(GroupDao.Properties.Name.eq(groupName))
                                .buildDelete().executeDeleteWithoutDetachingEntities();
                        OrganizerApplication.getDaoSession().clear();

                        //message about removing
                        Toast.makeText(OrganizerApplication.getContext(), groupName + getString(R.string.has_been_deleten_from_the_list), Toast.LENGTH_SHORT).show();

                        //extra actions when removing currently active group
                        SharedPreferences preferences = MainActivity.getInstance().getSharedPreferences(getString(R.string.settings_file), MainActivity.MODE_PRIVATE);
                        String currentGroup = preferences.getString(getString(R.string.settings_current_group),null);
                        if (currentGroup.equals(groupName)) {
                            SharedPreferences.Editor editor = preferences.edit();
                            editor.putString(getString(R.string.settings_current_group), null);
                            editor.apply();

                            //reload activity
                            Intent i = new Intent(MainActivity.getInstance(), MainActivity.class);
                            MainActivity.getInstance().finish();
                            startActivity(i);
                            return;
                        }
                        MainActivity.getInstance().reloadSidebarSubMenus();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // just do nothing
                    }
                });
        return builder.create();
    }

}
