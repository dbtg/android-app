package pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.activities.main.fragments;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.Locale;

import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.R;
import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.activities.main.Plan;
import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.activities.main.adapters.PlanRecyclerViewAdapter;
import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.helpers.ColorHelper;
import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.helpers.DateHelper;
import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.models.api.Block;
import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.models.viewpager.DayPlan;

import static android.content.ContentValues.TAG;

public class DayPlanFragment extends Fragment {
    //elements
    private TextView toolbarTitle;
    private RecyclerView mRecyclerView;
    RecyclerView.Adapter mAdapter;

    //current date
    private DayPlan dayPlan;
    public DayPlan getDayPlan() {
        return dayPlan;
    }

    //blocks list
    public ArrayList<Plan> mainPlan = new ArrayList<>();

    /**
     * Default constructor
     */
    public DayPlanFragment() {
    }

    /**
     * Construct fragment with certain blocks
     * @param dp model with blocks list
     */
    public DayPlanFragment(DayPlan dp) {
        dayPlan = dp;
        updateFragmentData();
    }

    /**
     * Load new plan for already initialized fragment
     * @param dp model with blocks list
     */
    public void loadNewPlan(DayPlan dp){
        dayPlan = dp;
        loadDay();
    }

    /**
     * Update the date in toolbar title if the date is already set
     */
    public void updateFragmentData(){
        if (toolbarTitle == null){
            return;
        }

        toolbarTitle.setText(dayPlan.getCalendar().getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.getDefault()) +
                ", " + dayPlan.getCalendar().get(Calendar.DATE) + "." + (dayPlan.getCalendar().get(Calendar.MONTH) + 1) + "." + dayPlan.getCalendar().get(Calendar.YEAR));
    }

    /**
     * Actions on fragment create
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView =inflater.inflate(R.layout.content_main, container, false);

        //set title
        toolbarTitle = (TextView)rootView.findViewById(R.id.toolbar_title);
        toolbarTitle.setText(R.string.pick_a_plan);

        //prepare recyclerview
        mainPlan.clear();
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.main_list);
        registerForContextMenu(mRecyclerView);

        //prepare adapter and decoration
        mAdapter = new PlanRecyclerViewAdapter(mainPlan);
        DividerItemDecoration mDividerItemDecoration = new DividerItemDecoration(mRecyclerView.getContext(),getResources().getConfiguration().orientation);
        mDividerItemDecoration.setDrawable(ContextCompat.getDrawable(mRecyclerView.getContext(), R.drawable.list_element_border));

        //assing adapter to recycler
        mRecyclerView.addItemDecoration(mDividerItemDecoration);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        //load data
        updateFragmentData();
        if (dayPlan.getGroupName() != null) {
            loadDay();
        }
        return rootView;
    }

    /**
     * Load blocks to recycler view
     */
    public void loadDay(){
        boolean itemsChanged = false;

        //generate fragment date
        int nextBlock = 0;
        mainPlan.clear();
        for (pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.models.database.Block block : this.getDayPlan().getBlocks()) {
            if (block.getBlock() > nextBlock) {
                for (int i = nextBlock; i < block.getBlock(); i++) {
                    Plan test = new Plan("Wolne", "", "", DateHelper.getBlockStart(i), DateHelper.getBlockEnd(i), Color.argb(50,250,250,0));
                    mainPlan.add(test);
                }
            }
            nextBlock = block.getBlock() + 1;
            Plan planElement = new Plan(block.getSubject(), block.getType(), block.getRoom(), block.getBlockStart(), block.getBlockEnd(), block.getColor());
            mainPlan.add(planElement);

        }

        //fill the rest of day with free blocks
        for (int i = nextBlock; i < 7; i++){
            Plan test= new Plan("Wolne","","", DateHelper.getBlockStart(i), DateHelper.getBlockEnd(i),Color.argb(50,250,250,0));
            mainPlan.add(test);
        }

        itemsChanged = true;

        if (itemsChanged) {
            mAdapter.notifyItemInserted(mainPlan.size());
            mAdapter.notifyDataSetChanged();
        }
    }

    /**
     * Initializing helper
     * @param dayPlan
     * @return
     */
    public static Fragment instantiateWithArgs(DayPlan dayPlan) {
        return new DayPlanFragment(dayPlan);
    }

    /**
     * Reload RecyclerView after changing data
     */
    public void refreshRecyclerView() {
        if (mRecyclerView != null) {
            mRecyclerView.setEnabled(false);
            mRecyclerView.invalidate();
            mAdapter.notifyDataSetChanged();
        }
    }
}
