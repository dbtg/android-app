package pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.models.database;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;

import java.util.Date;
import org.greenrobot.greendao.annotation.Generated;

import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.helpers.DateHelper;

@Entity
public class Block {

    @Id
    private Integer id;

    @NotNull
    private Integer groupId;

    @NotNull
    private Integer block;

    @NotNull
    private String date;

    private String type;

    private String extra;

    private String room;

    private String lecturers;

    private Integer color;

    @NotNull
    private String _short;

    @NotNull
    private String subject;

    @Generated(hash = 760942400)
    public Block(Integer id, @NotNull Integer groupId, @NotNull Integer block,
            @NotNull String date, String type, String extra, String room,
            String lecturers, Integer color, @NotNull String _short,
            @NotNull String subject) {
        this.id = id;
        this.groupId = groupId;
        this.block = block;
        this.date = date;
        this.type = type;
        this.extra = extra;
        this.room = room;
        this.lecturers = lecturers;
        this.color = color;
        this._short = _short;
        this.subject = subject;
    }

    @Generated(hash = 1192728677)
    public Block() {
    }

    public String getBlockStart(){
        return DateHelper.getBlockStart(this.block);
    }
    public String getBlockEnd(){
        return DateHelper.getBlockEnd(this.block);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBlock() {
        return block;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public void setBlock(Integer block) {
        this.block = block;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getLecturers() {
        return lecturers;
    }

    public void setLecturers(String lecturers) {
        this.lecturers = lecturers;
    }

    public String get_short() {
        return _short;
    }

    public void set_short(String _short) {
        this._short = _short;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Integer getColor() {
        return this.color;
    }

    public void setColor(Integer color) {
        this.color = color;
    }
}
