package pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.helpers;

import android.content.SharedPreferences;

import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.OrganizerApplication;
import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.R;
import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.activities.MainActivity;

public class PreferencesHelper {
    /**
     * Get current/default group from shared preferences
     * @return
     */
    public static String getDefaultGroup(){
        SharedPreferences preferences = OrganizerApplication.getContext().getSharedPreferences(OrganizerApplication.getContext().getString(R.string.settings_file), MainActivity.MODE_PRIVATE);
        String groupName = preferences.getString(OrganizerApplication.getContext().getString(R.string.settings_current_group), null);
        return groupName;
    }

    /**
     * Set current/default group in shared preferences for later use
     * @param groupName
     */
    public static void setDefaultGroup(String groupName){
        SharedPreferences preferences = OrganizerApplication.getContext().getSharedPreferences(OrganizerApplication.getContext().getString(R.string.settings_file), MainActivity.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(OrganizerApplication.getContext().getString(R.string.settings_current_group),groupName);
        editor.apply();
    }
}
