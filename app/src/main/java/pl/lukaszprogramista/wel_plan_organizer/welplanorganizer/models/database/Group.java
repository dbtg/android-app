package pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.models.database;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Index;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.OrderBy;
import org.greenrobot.greendao.annotation.ToMany;

import java.util.List;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.DaoException;
import org.greenrobot.greendao.annotation.Unique;

import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.OrganizerApplication;

/**
 * Group GreenDao database model
 */
@Entity(
        indexes = {
                @Index(value = "name", unique = true)
        }
)
public class Group {
    @Id
    private Integer id;

    @NotNull
    @Unique
    private String name;

    @ToMany(referencedJoinProperty = "groupId")
    @OrderBy("date ASC, block ASC")
    private List<Block> blocks;

    /** Used to resolve relations */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    @Generated(hash = 1591306109)
    private transient GroupDao myDao;

    @Generated(hash = 1708317296)
    public Group(Integer id, @NotNull String name) {
        this.id = id;
        this.name = name;
    }

    @Generated(hash = 117982048)
    public Group() {
    }

    /**
     * @return local group id
     */
    public Integer getId() {
        return id;
    }

    /**
     * set local group id
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return group name
     */
    public String getName() {
        return name;
    }

    /**
     * set group name
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * To-many relationship, resolved on first access (and after reset).
     * Changes to to-many relations are not persisted, make changes to the target entity.
     */
    @Generated(hash = 495380914)
    public List<Block> getBlocks() {
        if (blocks == null) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            BlockDao targetDao = daoSession.getBlockDao();
            List<Block> blocksNew = targetDao._queryGroup_Blocks(id);
            synchronized (this) {
                if (blocks == null) {
                    blocks = blocksNew;
                }
            }
        }
        return blocks;
    }

    /** Resets a to-many relationship, making the next get call to query for a fresh result. */
    @Generated(hash = 1731085026)
    public synchronized void resetBlocks() {
        blocks = null;
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }

    public static Group getGroupByName(String name){
        List<Group> groups = OrganizerApplication.getDaoSession().getGroupDao().queryBuilder().where(GroupDao.Properties.Name.eq(name)).list();
        if (groups.size() == 0){
            return null;
        }
        return groups.get(0);
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 1333602095)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getGroupDao() : null;
    }

}
