package pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.services;

import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.models.api.RestListResponse;

import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.models.api.RestPlanResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ApiService {
    @GET("/list")
    Call<RestListResponse> getGroups();

    @GET("/plan/{group}")
    Call<RestPlanResponse> getPlan(@Path("group") String group);
}
