package pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.models.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.helpers.DateHelper;

public class Block {

    @SerializedName("block")
    @Expose
    private Integer block;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("extra")
    @Expose
    private String extra;
    @SerializedName("room")
    @Expose
    private String room;
    @SerializedName("lecturers")
    @Expose
    private String lecturers;
    @SerializedName("short")
    @Expose
    private String _short;
    @SerializedName("subject")
    @Expose
    private String subject;

    public Integer getBlock() {
        return block;
    }

    public void setBlock(Integer block) {
        this.block = block;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getLecturers() {
        return lecturers;
    }

    public void setLecturers(String lecturers) {
        this.lecturers = lecturers;
    }

    public String getShort() {
        return _short;
    }

    public void setShort(String _short) {
        this._short = _short;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBlockStart(){
        return DateHelper.getBlockStart(this.block);
    }
    public String getBlockEnd(){
        return DateHelper.getBlockEnd(this.block);
    }
}