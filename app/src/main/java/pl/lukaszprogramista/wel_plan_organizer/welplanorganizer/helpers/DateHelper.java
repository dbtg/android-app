package pl.lukaszprogramista.wel_plan_organizer.welplanorganizer.helpers;

import java.util.Calendar;

public class DateHelper {

    private static String[] startBlocks = {"8:00", "9:50", "11:40", "13:30", "15:45", "17:35", "19:25"};
    private static String[] endBlocks = {"9:35", "11:25", "13:15", "15:05", "17:20", "19:10", "21:00"};

    /**
     * Convert Block index to block start time
     * @param blockIndex
     * @return
     */
    public static String getBlockStart(int blockIndex){
        return startBlocks[blockIndex];
    }

    /**
     * Convert Block index to block end time
     * @param blockIndex
     * @return
     */
    public static String getBlockEnd(int blockIndex){
        return endBlocks[blockIndex];
    }

    /**
     * Determine if both calendars are pointing to same day (ignore time and timezone)
     * @param c1
     * @param c2
     * @return
     */
    public static boolean compareCalendars(Calendar c1, Calendar c2){
        if (c1.get(Calendar.DATE) != c2.get(Calendar.DATE)){
            return false;
        }
        if (c1.get(Calendar.MONTH) != c2.get(Calendar.MONTH)){
            return false;
        }
        if (c1.get(Calendar.YEAR) != c2.get(Calendar.YEAR)){
            return false;
        }
        return true;
    }

}
